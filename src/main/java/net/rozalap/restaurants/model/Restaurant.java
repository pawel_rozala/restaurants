package net.rozalap.restaurants.model;

import net.rozalap.restaurants.enums.FoodType;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * Created by rozalap on 2017-03-04.
 */
@Entity
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Version
    @ColumnDefault(value = "0")
    private int version;

    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "FOODTYPE", nullable = false)
    private FoodType foodType;

    @Column(nullable = false)
    private int locationX;

    @Column(nullable = false)
    private int locationY;

    public Restaurant() {
    }

    public Restaurant(String name, FoodType foodType, int locationX, int locationY) {
        this.name = name;
        this.foodType = foodType;
        this.locationX = locationX;
        this.locationY = locationY;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public int getLocationX() {
        return locationX;
    }

    public void setLocationX(int locationX) {
        this.locationX = locationX;
    }

    public int getLocationY() {
        return locationY;
    }

    public void setLocationY(int locationY) {
        this.locationY = locationY;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", version=" + version +
                ", name='" + name + '\'' +
                ", foodType=" + foodType +
                ", locationX=" + locationX +
                ", locationY=" + locationY +
                '}';
    }
}
