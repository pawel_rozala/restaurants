package net.rozalap.restaurants.services.impl;

import net.rozalap.restaurants.dto.RestaurantDTO;
import net.rozalap.restaurants.enums.FoodType;
import net.rozalap.restaurants.model.Restaurant;
import net.rozalap.restaurants.repositories.RestaurantRepository;
import net.rozalap.restaurants.services.RestaurantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by rozalap on 2017-03-05.
 */
@Service
public class RestaurantServiceImpl implements RestaurantService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Override
    public Restaurant getRestaurant(int id) {
        log.debug("Get restaurant with id " + id);
        return restaurantRepository.findOne(id);
    }

    @Override
    public void createRestaurant(Restaurant restaurant) {
        log.debug("Create restaurant: " + restaurant.toString());
        restaurantRepository.save(restaurant);

    }

    @Override
    public void updateRestaurant(Restaurant modifiedRestaurant) {
        //the same method in restaurantRepository, but we'll distinct them for the sake of clarity in the service API
        log.debug("Update restaurant: " + modifiedRestaurant.toString());
        restaurantRepository.save(modifiedRestaurant);

    }

    @Override
    public void deleteRestaurant(int id) {
        log.debug("Delete restaurant with id " + id);
        restaurantRepository.delete(id);
    }

    @Override
    public List<RestaurantDTO> findNearestRestaurants(int locationX, int locationY) {
        return findNearestRestaurantsByFoodType(locationX, locationY, null);
    }

    @Override
    public List<RestaurantDTO> findNearestRestaurantsByFoodType(int userLocationX, int userLocationY, FoodType foodType) {
        log.debug("Find restaurants near " + userLocationX + ", " + userLocationY + " with food " + foodType);

        List<RestaurantDTO> sortedFoodTypeRestaurantList = new ArrayList<RestaurantDTO>();
        List<RestaurantDTO> sortedOtherRestaurantList = new ArrayList<RestaurantDTO>();

        List<Restaurant> restaurants = (List<Restaurant>) restaurantRepository.findAll();

        for (Restaurant restaurant : restaurants) {
            //We use a DTO object to hold the distance from location and to use list sorting
            RestaurantDTO restaurantDTO = createRestaurantDTO(restaurant);
            double distance = Math.hypot(userLocationX - restaurant.getLocationX(), userLocationY - restaurant.getLocationY());
            restaurantDTO.setDistanceFromUser(distance);

            //We filter out the restaurants with given foodType (if any), to put them first on the result list
            if (foodType != null && foodType.equals(restaurant.getFoodType())) {
                sortedFoodTypeRestaurantList.add(restaurantDTO);
            } else {
                sortedOtherRestaurantList.add(restaurantDTO);
            }
        }

        Collections.sort(sortedFoodTypeRestaurantList);
        Collections.sort(sortedOtherRestaurantList);

        sortedFoodTypeRestaurantList.addAll(sortedOtherRestaurantList);

        return sortedFoodTypeRestaurantList;

    }

    private RestaurantDTO createRestaurantDTO(Restaurant restaurant) {
        RestaurantDTO restaurantDTO = new RestaurantDTO();
        restaurantDTO.setId(restaurant.getId());
        restaurantDTO.setName(restaurant.getName());
        restaurantDTO.setFoodType(restaurant.getFoodType());
        restaurantDTO.setLocationX(restaurant.getLocationX());
        restaurantDTO.setLocationY(restaurant.getLocationY());
        return restaurantDTO;
    }
}
