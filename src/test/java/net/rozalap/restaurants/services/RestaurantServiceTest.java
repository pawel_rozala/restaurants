package net.rozalap.restaurants.services;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import net.rozalap.restaurants.dto.RestaurantDTO;
import net.rozalap.restaurants.enums.FoodType;
import net.rozalap.restaurants.model.Restaurant;
import net.rozalap.restaurants.repositories.RestaurantRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by rozalap on 2017-03-06.
 */
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@RunWith(SpringRunner.class)
@SpringBootTest
@DatabaseSetup(RestaurantServiceTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = {RestaurantServiceTest.DATASET})
@DirtiesContext
public class RestaurantServiceTest {

    static final String DATASET = "classpath:datasets/restaurants.xml";

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private RestaurantService restaurantService;

    @Test
    public void testGetRestaurant() throws Exception {
        Restaurant restaurant = restaurantService.getRestaurant(1);
        assertThat(restaurant.getId() == 1).isTrue();
        assertThat(restaurant.getName().equals("Green Dragon")).isTrue();
        assertThat(FoodType.CHINESE.equals(restaurant.getFoodType())).isTrue();
    }

    @Test
    public void testCreateRestaurant() throws Exception {
        List<Restaurant> restaurantList = (List<Restaurant>) restaurantRepository.findAll();
        assertThat(restaurantList).hasSize(10);

        Restaurant newRestaurant = new Restaurant("Madrid", FoodType.SPANISH, 6, 6);
        restaurantService.createRestaurant(newRestaurant);

        restaurantList = (List<Restaurant>) restaurantRepository.findAll();
        assertThat(restaurantList).hasSize(11);
    }

    @Test
    public void testUpdateRestaurant() throws Exception {
        Restaurant restaurant3 = restaurantService.getRestaurant(3);
        assertThat(FoodType.ITALIAN.equals(restaurant3.getFoodType())).isTrue();
        restaurant3.setFoodType(FoodType.FRENCH);
        restaurantService.updateRestaurant(restaurant3);
        Restaurant restaurantAfterUpdate = restaurantService.getRestaurant(3);
        assertThat(FoodType.FRENCH.equals(restaurantAfterUpdate.getFoodType())).isTrue();
    }

    @Test
    public void testDeleteRestaurant() throws Exception {
        List<Restaurant> restaurantList = (List<Restaurant>) restaurantRepository.findAll();
        assertThat(restaurantList).hasSize(10);
        restaurantService.deleteRestaurant(2);
        restaurantList = (List<Restaurant>) restaurantRepository.findAll();
        assertThat(restaurantList).hasSize(9);
    }

    @Test
    public void testFindNearestRestaurantNoFoodType() throws Exception {
        List<RestaurantDTO> list = restaurantService.findNearestRestaurants(0, 0);
        assertThat(list.size() == 10).isTrue();
        assertThat(list.get(0).getFoodType().equals(FoodType.INDIAN)).isTrue();
    }

    @Test
    public void testFindNearestRestaurantWithFoodType() throws Exception {
        List<RestaurantDTO> list = restaurantService.findNearestRestaurantsByFoodType(0, 0, FoodType.SPANISH);
        assertThat(list.size() == 10).isTrue();
        assertThat(list.get(0).getFoodType().equals(FoodType.SPANISH)).isTrue();
        assertThat(list.get(0).getName().equals("Barcelona")).isTrue();
        assertThat(list.get(1).getFoodType().equals(FoodType.SPANISH)).isTrue();
    }
}