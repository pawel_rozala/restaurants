package net.rozalap.restaurants.dto;

import net.rozalap.restaurants.model.Restaurant;

/**
 * Created by rozalap on 2017-03-05.
 */
public class RestaurantDTO extends Restaurant implements Comparable<RestaurantDTO> {

    double distanceFromUser;

    public double getDistanceFromUser() {
        return distanceFromUser;
    }

    public void setDistanceFromUser(double distanceFromUser) {
        this.distanceFromUser = distanceFromUser;
    }

    @Override
    public int compareTo(RestaurantDTO other) {
        return Double.compare(this.distanceFromUser, other.distanceFromUser);
    }

    @Override
    public String toString() {
        return "RestaurantDTO{ " +
                super.toString()+
                " distanceFromUser=" + distanceFromUser +
                '}';
    }
}
