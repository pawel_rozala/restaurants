package net.rozalap.restaurants.repositories;

import net.rozalap.restaurants.model.Restaurant;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rozalap on 2017-03-04.
 */
public interface RestaurantRepository extends CrudRepository<Restaurant, Integer> {

}

