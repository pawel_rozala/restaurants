package net.rozalap.restaurants.enums;

/**
 * Created by rozalap on 2017-03-04.
 */
public enum FoodType {
    CHINESE,
    INDIAN,
    FRENCH,
    ITALIAN,
    SPANISH,
    POLISH
}
