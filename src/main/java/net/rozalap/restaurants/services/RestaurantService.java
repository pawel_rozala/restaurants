package net.rozalap.restaurants.services;

import net.rozalap.restaurants.dto.RestaurantDTO;
import net.rozalap.restaurants.enums.FoodType;
import net.rozalap.restaurants.model.Restaurant;

import java.util.List;

/**
 * Created by rozalap on 2017-03-04.
 */
public interface RestaurantService {

    /**
     * Get restaurant by it's id
     *
     * @param id
     * @return
     */
    Restaurant getRestaurant(int id);

    /**
     * Create new restaurant
     *
     * @param newRestaurant
     */
    void createRestaurant(Restaurant newRestaurant);

    /**
     * Save modified restaurant
     *
     * @param modifiedRestaurant
     */
    void updateRestaurant(Restaurant modifiedRestaurant);

    /**
     * Delete restaurant with given id
     *
     * @param id
     */
    void deleteRestaurant(int id);

    /**
     * List restaurants in order of proximity to given location
     *
     * @param locationX
     * @param locationY
     * @return
     */
    List<RestaurantDTO> findNearestRestaurants(int locationX, int locationY);

    /**
     * List restaurants in order of proximity to given location (restaurants with given foodType will be listed first)
     *
     * @param locationX
     * @param locationY
     * @param foodType
     * @return
     */
    List<RestaurantDTO> findNearestRestaurantsByFoodType(int locationX, int locationY, FoodType foodType);
}
